import os
from tfserver import service_models
import numpy as np
import skimage
from skimage.io import imread
from dnn import abiou
import cv2
from dnn import plot
from tfserver.label import Label
from dnn.processing.image.rect_util import Rect
from dnn.processing.image import img_util as imgu
from PIL import Image
from skimage import exposure

# preprocessing ---------------------------------------
INPUT_SHAPE = (480, 640, 3)
FEATURE_SHAPES = [[30, 40, 16], [15, 20, 16], [8, 10, 16], [4, 5, 16]]
N_LAYERS = 4
ASPECT_RATIOS = (1, 2, 0.5)
ANCHORS = abiou.generate_anchors (FEATURE_SHAPES, INPUT_SHAPE, N_LAYERS, aspect_ratios = ASPECT_RATIOS)
CLASS_THRESHOLD = 0.5
IOU_THRESHOLD = 0.2
LABEL_IOU_THRESHOLD = 0.6
SOFT_NMS = True
NORMALIZE = False
LABEL = Label ({
    0:"background", 1: "Water", 2: "Soda", 4: "Juice"
})

def get_anchors ():
    return ANCHORS.copy ()

def detect_objects (image, predict_func, label):
    image = np.expand_dims(image, axis=0)
    classes, offsets = predict_func (image)
    image = np.squeeze (image, axis=0)
    classes = np.squeeze (classes)
    offsets = np.squeeze (offsets)
    anchors = get_anchors ()

    objects, indexes, scores_, offsets = abiou.nms (
        classes, offsets, anchors,
        class_threshold = CLASS_THRESHOLD, iou_threshold = IOU_THRESHOLD,
        soft_nms = SOFT_NMS, normalize = NORMALIZE
    )
    scores, class_names, class_ids, boxes = [], [], [], []
    for idx in indexes:
        anchor = anchors[idx]
        offset = offsets[idx]
        anchor += offset[0:4] # [xmin, xmax, ymin, ymax]
        boxes.append(anchor)
        category = int(objects[idx])
        class_ids.append(category)
        class_name = label.class_name (category)
        class_name = "%s: %0.2f" % (class_name, scores_[idx])
        print(class_name, anchor)
        class_names.append (class_name)
        scores.append (scores_[idx])
    return class_names, class_ids, boxes, scores

def evaluate (image, predict_func, label, show = False):
    arr = load_image (image)
    class_names, class_ids, boxes, scores = detect_objects (arr, predict_func, label)
    if show:
        image = imread (image)
        marked = image.copy ()
        for idx, class_name in enumerate (class_names):
            box = boxes [idx].astype (np.int32)
            cv2.putText (marked, class_name, (box [0], box [2] - 10), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0), 3)
            cv2.rectangle (marked, (box [0], box [2]), (box [1], box [3]), (0, 0, 255), 5)
        plot.hview (image, marked)
    return class_ids, boxes, scores, class_names

def contrast_stretching (x, r1 = 2, r2 = 98):
    p2, p98 = np.percentile (x, (r1, r2))
    return exposure.rescale_intensity (x, in_range = (p2, p98))

def load_image (image):
    if isinstance (image, str):
        image = imread (image)
    arr = skimage.img_as_float (image)
    # arr = exposure.equalize_adapthist (contrast_stretching (arr), clip_limit = 0.03)
    # arr = exposure.equalize_adapthist (arr, clip_limit = 0.03)
    return arr


# servicer ---------------------------------------
class Model (service_models.Model):
    def evaluate (self, image, show = False):
        return evaluate (image, self.predict, self.ds.labels [0], show)

    def preprocess (self, image):
        return load_image (image)


# load ---------------------------------------
def load (model_path, **config):
    return Model (model_path, **config)
